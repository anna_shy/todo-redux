import React, { useState } from 'react';
import { useDispatch } from "react-redux";
import * as actions from "../store/actions";
import { Button, FormControl, InputGroup } from "react-bootstrap";

const AddNewTask = () => {
  const [taskTitle, setTaskTitle] = useState('');
  const dispatch = useDispatch();

  const handleTaskTitleChange = (e) => {
    setTaskTitle(e.target.value);
  }

  const handleTaskSubmit = () => {
    dispatch(actions.addTask({
      title: taskTitle
    }));
    setTaskTitle('');
  }

  return (
    <InputGroup>
      <FormControl placeholder="А напиши заданице" value={taskTitle} onChange={e => handleTaskTitleChange(e)} />
      <InputGroup.Append>
        <Button onClick={handleTaskSubmit}>Сохранить</Button>
      </InputGroup.Append>
    </InputGroup>
  )
}

export default AddNewTask;